#include <stdio.h>

int main()
{
	printf("Item #1");
	int tam,i,opcion,tempVector;
	printf("Ingrese el tamaño de la matriz\nRecordando que  la matriz sera simetrica\n");
	scanf("%d", &tam);
	printf("Llenado de matrices\n");
	int matriz1[tam][tam];
	int matriz2[tam][tam];
	for(i=0;i<tam;i++){
		for(int o=0;o<tam;o++){
				printf("Ingrese un valor para la matriz A en la posicion %d,%d:\n",i+1,o+1);
				scanf("%d", &matriz1[i][o]);
		}
	}
	for(i=0;i<tam;i++){
		for(int o=0;o<tam;o++){
				printf("Ingrese un valor para la matriz B en la posicion %d,%d:\n",i+1,o+1);
				scanf("%d", &matriz2[i][o]);
		}
	}
	printf("Multiplicar las  matrices\n");
	for(i=0;i<tam;i++){
		for(int o=0;o<tam;o++){
				printf("%d\t", (matriz1[i][o]*matriz2[i][o]));
		}
		printf("\n");
	}
	
	printf("Item # 2");
	for(i=0;i<tam;i++){
		for(int o=0;o<tam;o++){
				printf("Ingrese un valor para la matriz en la posicion %d,%d:\n",i+1,o+1);
				scanf("%d", &matriz1[i][o]);
		}
	}
	for(i=0;i<tam;i++){
		for(int o=0;o<tam;o++){
				matriz2[o][i]=matriz1[i][o];
		}
	}
	for(i=0;i<tam;i++){
		for(int o=0;o<tam;o++){
				printf("%d\t", matriz1[i][o]);
		}
		printf("\n");
	}
	printf("\n");
	for(i=0;i<tam;i++){
		for(int o=0;o<tam;o++){
				printf("%d\t", matriz2[i][o]);
		}
		printf("\n");
	}
	int tamV;
	do{
		printf("\nIngrese un valor para el tamano del vector");
		scanf("%d\n",&tamV);
	}while(tamV<=0);
	int vector[(tamV+1)];
	for (i=0;i<tamV;i++){
		printf("Ingrese un valor a la posicion %d:\n", (i+1));
		scanf("%d\n", &vector[i]);
	}
	int valor;
	do{
		printf("Opciones\n1.Agregar dato en posicion especifica\n2.Buscar valor y obtener su posicion\n3.Editar una posicion\4.Eliminar una posicion\n");
		scanf("%d\n", &opcion);
		switch(opcion){
		case 1:
			do{
				printf("Ingrese la posicion en la cual desea agregar el valor\n");
				scanf("%d\n", &tempVector);
			}while(tempVector <= 0 || tempVector>(tamV+1));
			int valor;
			scanf("Ingrese nuevo valor para el vector \n%d\n", &valor);
			vector[tempVector] = valor;
			printf("Posicion cambiada exitosamente");
		break;
		case 2:	
		printf("Ingrese el valor que desea buscar en el vector\n");
		scanf("%d\n",&valor);
		for (i=0;i<tamV;i++){
			if(vector[i]==valor){
			printf("Posicion Encontrada %d\n",(i+1));
			}
		}
		break;
		case 3:
		do{
			printf("Ingrese la posicion en la cual desea editar el valor\n");
			scanf("%d\n", &tempVector);
		}while(tempVector <= 0 || tempVector>(tamV+1));
		scanf("Ingrese nuevo valor para el vector \n%d\n", &valor);
			vector[tempVector] = valor;
			printf("Posicion cambiada exitosamente");
		break;
		case 4:
		do{
			printf("Ingrese la posicion en la cual desea eliminar el valor\n");
			scanf("%d\n", &tempVector);
		}while(tempVector <= 0 || tempVector>(tamV+1));
			vector[tempVector] = 0;
			printf("Posicion eliminada exitosamente\n");
		break;
		printf("Fin del Menu \n");
		}
	}while(opcion>=1 && opcion<=4);
	int suma;
	printf("Item # 4 guia\n");
	for(i=0;i<tam;i++){
		suma =0;
		for(int o=0;o<tam;o++){
				suma = suma + matriz1[i][o];
		}
		printf("La suma de la fila %d es %d",(i+1),suma);
	}
	for(i=0;i<tam;i++){
		suma =0;
		for(int o=0;o<tam;o++){
				suma = suma + matriz1[i][o];
		}
		printf("La suma de la fila %d es %d",(i+1),suma);
	}
	for(i=0;i<tam;i++){
		suma =0;
		for(int o=0;o<tam;o++){
				suma = suma + matriz1[o][i];
		}
		printf("La suma de la columna %d es %d",(i+1),suma);
	}
	suma =0;
	int contador = (tam -1);
	for(i=0;i<tam;i++){
		for(int o=0;o<tam;o++){
				if(o==contador){
					suma = suma + matriz1[i][o];
					contador--;
				}
		}
		printf("La suma de la diagonal es %d",suma);
	}
	return 0;
}

